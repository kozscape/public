# KozScape

## Play!

1. Visit the [downloads](https://bitbucket.org/kozscape/public/downloads/) section on the left and download `kozscape.jar`
2. Double click to run the jar file - Preferably run it with the latest version java
3. After loading the game, use your desired credentials to log in (It will automatically create an account)
4. Enjoy

## Logging issues

If you run into an issue, visit the `Issues` tab on the left of this site and create a new issue.
Please put as much detail as possible into the ticket so that we can track down the bug and fix it.